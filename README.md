# titanic-prediction

Using machine learning to predict the survivors among Titanic passengers. See https://www.kaggle.com/c/titanic for details.